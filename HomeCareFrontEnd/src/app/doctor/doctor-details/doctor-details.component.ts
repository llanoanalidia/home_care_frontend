import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Doctor } from 'src/app/interfaces/doctor.model';
import { RepositoryService } from '../../shared/services/repository.service';
import { Router,ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-doctor-details',
  templateUrl: './doctor-details.component.html',
  styleUrls: ['./doctor-details.component.css']
})
export class DoctorDetailsComponent implements OnInit {
  public doctor: any;
  public doctorDetail: any;
  public breadCrumbs: any = [];
  
  constructor(private repository: RepositoryService,  private router: Router, private activeRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this.doctorDetail = new FormGroup({
      firstName: new FormControl ({value: '', disabled: true}, [Validators.required]),
      middle: new FormControl ({value: '', disabled: true}, [Validators.required]),
      lastName: new FormControl ({value: '', disabled: true}, [Validators.required]),
      dateOfBirth: new FormControl ({value: '', disabled: true}, [Validators.required]),
      npi: new FormControl ({value: '', disabled: true}, [Validators.required]),
      phone1: new FormControl ({value: '', disabled: true}, [Validators.required]),
      phone2: new FormControl ({value: '', disabled: true}, [Validators.required]),
      eMail: new FormControl ({value: '', disabled: true}, [Validators.required]),
      user: new FormGroup ({
          userName: new FormControl ({value: '', disabled: true}, [Validators.required]),
          // pasword: new FormControl ({value: '', disabled: true}, [Validators.required]),
      })
    });
    this.getDoctorById();
  }

  private getDoctorById = () => {
    let doctorId: string = this.activeRoute.snapshot.params['id'];
    // console.log("Ruta",this.activeRoute.snapshot.url);
    // this.breadCrumbs = this.activeRoute.snapshot.url;
    let doctorByIdUrl: string = `api/doctor/${doctorId}`;
  
    this.repository.getData(doctorByIdUrl)
      .subscribe(res => {
        this.doctor = res as Doctor;
        this.doctorDetail.patchValue(this.doctor);
      },)
  }

}
