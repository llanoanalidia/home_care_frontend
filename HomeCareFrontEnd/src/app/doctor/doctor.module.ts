import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgxMaskModule, IConfig } from 'ngx-mask'
import { DoctorListComponent } from './doctor-list/doctor-list.component';
import { RouterModule } from '@angular/router';
import {ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { SharedModule } from './../shared/shared.module';
import { DoctorCreateComponent } from './doctor-create/doctor-create.component';
import { DoctorUpdateComponent } from './doctor-update/doctor-update.component';
import { DoctorDetailsComponent } from './doctor-details/doctor-details.component';

const doctorMaskConfig: Partial<IConfig> = {
  validation: false,
};

@NgModule({
  declarations: [DoctorListComponent, DoctorCreateComponent, DoctorUpdateComponent,DoctorDetailsComponent],
  imports: [
    CommonModule,
    SharedModule,
    ReactiveFormsModule,
    FormsModule,
    NgxMaskModule.forRoot(doctorMaskConfig),
    RouterModule.forChild([
      { path: 'list', component: DoctorListComponent },
      { path: 'details/:id', component: DoctorDetailsComponent },
      { path: 'create', component: DoctorCreateComponent},
      { path: 'update/:id',component:DoctorUpdateComponent}
    ]),
  ]
})
export class DoctorModule { }
