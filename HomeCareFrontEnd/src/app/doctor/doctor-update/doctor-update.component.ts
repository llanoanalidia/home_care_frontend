import { Component, OnInit } from '@angular/core';
import { RepositoryService } from './../../shared/services/repository.service';
import { doctorForCreation } from './../../interfaces/doctorForCreation.model';
import { userForCreation } from './../../interfaces/userForCreation.model';
import { Router, ActivatedRoute } from '@angular/router';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Doctor } from 'src/app/interfaces/doctor.model';

@Component({
  selector: 'app-doctor-update',
  templateUrl: './doctor-update.component.html',
  styleUrls: ['./doctor-update.component.css'],
})
export class DoctorUpdateComponent implements OnInit {
  public doctorForm: any;
  public doctor: any;

  constructor(
    private repository: RepositoryService,
    private router: Router,
    private activeRoute: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.doctorForm = new FormGroup({
      firstName: new FormControl('', [Validators.required]),
      middle: new FormControl('', [Validators.required]),
      lastName: new FormControl('', [Validators.required]),
      dateOfBirth: new FormControl('', [Validators.required]),
      npi: new FormControl('', [Validators.required]),
      phone1: new FormControl('', [Validators.required]),
      phone2: new FormControl('', [Validators.required]),
      eMail: new FormControl('', [Validators.required]),
      user: new FormGroup({
        userName: new FormControl('', [Validators.required]),
        pasword: new FormControl('', [Validators.required]),
      }),
    });

    this.getDoctorById();
  }
  private getDoctorById = () => {
    let doctorId: string = this.activeRoute.snapshot.params['id'];
    let doctorByIdUrl: string = `api/doctor/${doctorId}`;

    this.repository.getData(doctorByIdUrl).subscribe((res) => {
      this.doctor = res as Doctor;
      this.doctorForm.patchValue(this.doctor);
    });
  };
  public updateDoctor = (event:any,doctorFormValue: any) => {
    event.target.classList.add('was-validated')
    if (this.doctorForm.valid) {
      this.executeDoctortUpdate(event,doctorFormValue);
    }
  };

  private executeDoctortUpdate = (event:any,doctorFormValue: any) => {
    this.doctor.firstName = doctorFormValue.firstName;
    this.doctor.middle = doctorFormValue.middle;
    this.doctor.lastName = doctorFormValue.lastName;
    this.doctor.dateOfBirth = doctorFormValue.dateOfBirth;
    this.doctor.npi = doctorFormValue.npi;
    this.doctor.phone1 = doctorFormValue.phone1;
    this.doctor.phone2 = doctorFormValue.phone2;
    this.doctor.eMail = doctorFormValue.eMail;
    this.doctor.user.userName = doctorFormValue.user.userName;
    this.doctor.user.pasword = doctorFormValue.user.pasword;
    if( event.target.checkValidity() ){
      let apiUrl = `api/doctor/${this.doctor.idDoctor}`;
      this.repository.update(apiUrl, this.doctor).subscribe((res) => {
        ($('#successModal') as any).modal({ show: true });
      });
    }else{
      event.target.preventDefault()
      event.target.stopPropagation()
    }
  };
}
