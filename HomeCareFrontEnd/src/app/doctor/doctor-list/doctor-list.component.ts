import { Component, OnInit } from '@angular/core';
import { Doctor } from 'src/app/interfaces/doctor.model';
import { RepositoryService } from './../../shared/services/repository.service';
import { Router } from '@angular/router';
import { ErrorHandlerService } from './../../shared/services/error-handler.service';

@Component({
  selector: 'app-doctor-list',
  templateUrl: './doctor-list.component.html',
  styleUrls: ['./doctor-list.component.css'],
})
export class DoctorListComponent implements OnInit {
  public doctors: Doctor[] = [];

  constructor(
    private repository: RepositoryService,
    private errorHandler: ErrorHandlerService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.getAllUserDoctors();
  }
  public getAllUserDoctors = () => {
    let apiAddress: string = 'api/doctor';
    this.repository.getData(apiAddress).subscribe((res) => {
      this.doctors = res as Doctor[];
    });
  };
  public redirectToUpdatePage = (id: any) => {
    const updateUrl: string = `/doctor/update/${id}`;
    this.router.navigate([updateUrl]);
  };
  public redirectToDetailPage = (id: any) => {
    const detailUrl: string = `/doctor/details/${id}`;
    this.router.navigate([detailUrl]);
  };
  
  public DeleteDoctor = (e: any, id: any) => {
    e.preventDefault();
    localStorage.setItem('deteleRoute',`api/doctor/${id}`);
    ($('#deleteModal') as any).modal({ show: true });
  };
}
