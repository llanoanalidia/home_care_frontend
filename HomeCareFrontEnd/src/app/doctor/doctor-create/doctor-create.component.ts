import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { RepositoryService } from './../../shared/services/repository.service';
import { doctorForCreation } from './../../interfaces/doctorForCreation.model';
import { userForCreation } from './../../interfaces/userForCreation.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-doctor-create',
  templateUrl: './doctor-create.component.html',
  styleUrls: ['./doctor-create.component.css'],
})
export class DoctorCreateComponent implements OnInit {
  public doctorForm: any;

  constructor(private repository: RepositoryService, private router: Router) {}

  ngOnInit(): void {
    this.doctorForm = new FormGroup({
      roleId: new FormControl('', [Validators.required]),
      userName: new FormControl('', [Validators.required]),
      password: new FormControl('', [Validators.required]),
      firstName: new FormControl('', [Validators.required]),
      middle: new FormControl('', [Validators.required]),
      lastName: new FormControl('', [Validators.required]),
      dateOfBirth: new FormControl('', [Validators.required]),
      socialSecurityNumber: new FormControl('', [Validators.required]),
      gender: new FormControl('', [Validators.required]),
      phone1: new FormControl('', [Validators.required]),
      phone2: new FormControl('', [Validators.required]),
      email: new FormControl('', [Validators.required]),
      npi: new FormControl('', [Validators.required]),
      country: new FormControl('', []),
      zipcode: new FormControl('', []),
      state: new FormControl('', []),
      city: new FormControl('', []),
      adress1: new FormControl('', []),
      adress2: new FormControl('', []),
    });
  }

  public createPatient = (event: any, doctorFormValue: any) => {
    event.target.classList.add('was-validated');
    if (this.doctorForm.valid) {
      this.executeDoctorCreation(event, doctorFormValue);
    }
  };
  public executeDoctorCreation = (event: any, doctorFormValue: any) => {
    console.log('event: ', event.target);
    const user: userForCreation = {
      roleId: 4,
      userName: doctorFormValue.userName,
      pasword: doctorFormValue.password,
    };

    const doctor: doctorForCreation = {
      firstName: doctorFormValue.firstName,
      middle: doctorFormValue.middle,
      lastName: doctorFormValue.lastName,
      dateOfBirth: doctorFormValue.dateOfBirth,
      phone1: doctorFormValue.phone1,
      phone2: doctorFormValue.phone2,
      eMail: doctorFormValue.email,
      npi: doctorFormValue.npi,
      user: user,
    };

    // Loop over them and prevent submission
    // Array.prototype.slice.call(forms)
    //   .forEach(function (form) {
    //     form.addEventListener('submit', function (event:any) {
    //       if (!form.checkValidity()) {
    //         event.preventDefault()
    //         event.stopPropagation()
    //       }

    //       form.classList.add('was-validated')
    //     }, false)
    //   })
    if (event.target.checkValidity()) {
      const apiUrl = 'api/doctor';
      this.repository.create(apiUrl, doctor).subscribe((res) => {
        ($('#successModal') as any).modal({ show: true });
      });
    } else {
      event.target.preventDefault()
      event.target.stopPropagation()
    }
    //end
  };
}
