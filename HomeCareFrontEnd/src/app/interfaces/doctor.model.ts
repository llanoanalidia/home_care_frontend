import { User } from "./user.model";

export interface Doctor {

    idDoctor: number;
    firstName :string;
    middle:string;
    lastName:string;
    dateOfBirth:Date;
    phone1:string;
    phone2:string;
    eMail:string;
    npi:string;
    user:User;

}