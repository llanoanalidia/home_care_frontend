import { userForCreation } from "./userForCreation.model";

export interface EmployeeForCreation {
    
    firstName: string;
    middle:string;
    lastName:string;
    dateOfBirth:Date;
    eMail:string;
    socialSecurityNumber:string;
    gender:string;
    status:string;
    // idRole:number;
    phone1: string,
    phone2: string,
    Address1:string;
    Address2:string;
    City:string;
    State:string;
    ZipCode:string;
    user:userForCreation;












}