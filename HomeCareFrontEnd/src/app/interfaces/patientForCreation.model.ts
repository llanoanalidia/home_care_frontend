import { demographicForCreation } from "./demographicForCreation.model";
import { User } from "./user.model";
import { userForCreation } from "./userForCreation.model";

export interface patientForCreation{
   
    firstName: string;
    middle: string;
    lastName: string;
    dateOfBirth: Date;
    socialSecurityNumber: string;
    gender: string;
    phone1: string;
    phone2: string;
    eMail: string;
    status: string;
    patientCode: string;
    user:userForCreation;
    demographic:demographicForCreation;


}