import { userForCreation } from "./userForCreation.model";

export interface doctorForCreation {

    
    firstName :string;
    middle:string;
    lastName:string;
    dateOfBirth:Date;
    phone1:string;
    phone2:string;
    eMail:string;
    npi:string;
    user:userForCreation;

}