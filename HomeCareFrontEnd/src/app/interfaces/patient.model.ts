import { Demographic } from "./demographic.model";
import { User } from "./user.model";

export interface Patient{
    
    idPatient:number;
    firstName: string;
    middle: string;
    lastName: string;
    dateOfBirth: Date;
    socialSecurityNumber: string;
    gender: string;
    phone1: string;
    phone2: string;
    eMail: string;
    status: string;
    demographic : Demographic;
    user: User;


}