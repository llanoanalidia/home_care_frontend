import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgxMaskModule, IConfig } from 'ngx-mask'
import { PatientListComponent } from './patient-list/patient-list.component';
import { SharedModule } from './../shared/shared.module';
import { RouterModule } from '@angular/router';
import {ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { PatientCreateComponent } from './patient-create/patient-create.component';
import { PatientUpdateComponent } from './patient-update/patient-update.component';

const patientMaskConfig: Partial<IConfig> = {
  validation: false,
};

@NgModule({
  declarations: [PatientListComponent, PatientCreateComponent, PatientUpdateComponent],
  imports: [
    CommonModule,
    SharedModule,
    ReactiveFormsModule,
    FormsModule,
    NgxMaskModule.forRoot(patientMaskConfig),
    RouterModule.forChild([
      { path: 'list', component: PatientListComponent },
      { path: 'create', component: PatientCreateComponent},
      { path: 'update/:id', component: PatientUpdateComponent}
    ]),
  ]
  })
export class PatientModule { }

