import { Component, OnInit } from '@angular/core';
import { RepositoryService } from './../../shared/services/repository.service';
import { Router, ActivatedRoute } from '@angular/router';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Patient } from 'src/app/interfaces/patient.model';

@Component({
  selector: 'app-patient-update',
  templateUrl: './patient-update.component.html',
  styleUrls: ['./patient-update.component.css'],
})
export class PatientUpdateComponent implements OnInit {
  public patientForm: any;
  public patient: any;

  constructor(
    private repository: RepositoryService,
    private router: Router,
    private activeRoute: ActivatedRoute
  ) {}

  ngOnInit() {
    this.patientForm = new FormGroup({
      firstName: new FormControl('', [Validators.required]),
      middle: new FormControl('', [Validators.required]),
      lastName: new FormControl('', [Validators.required]),
      dateOfBirth: new FormControl('', [Validators.required]),
      socialSecurityNumber: new FormControl('', [Validators.required]),
      gender: new FormControl('', [Validators.required]),
      phone1: new FormControl('', [Validators.required]),
      phone2: new FormControl('', [Validators.required]),
      eMail: new FormControl('', [Validators.required]),
      status: new FormControl('', [Validators.required]),
      patientCode: new FormControl('', [Validators.required]),
      user: new FormGroup({
        userName: new FormControl('', [Validators.required]),
        pasword: new FormControl('', [Validators.required]),
      }),
      demographic: new FormGroup({
        adress1: new FormControl('', [Validators.required]),
        adress2: new FormControl('', [Validators.required]),
        city: new FormControl('', [Validators.required]),
        state: new FormControl('', [Validators.required]),
        zipCode: new FormControl('', [Validators.required]),
        country: new FormControl('', [Validators.required]),
      }),
    });

    this.getPatientById();
  }

  private getPatientById = () => {
    let patientId: string = this.activeRoute.snapshot.params['id'];
    let patientByIdUrl: string = `api/patient/${patientId}`;

    this.repository.getData(patientByIdUrl).subscribe((res) => {
      this.patient = res as Patient;
      this.patientForm.patchValue(this.patient);
    });
  };

  public updatePatient = (event: any, patientFormValue: any) => {
    event.target.classList.add('was-validated');
    if (this.patientForm.valid) {
      this.executePatientUpdate(event, patientFormValue);
    }
  };

  private executePatientUpdate = (event: any, patientFormValue: any) => {
    this.patient.firstName = patientFormValue.firstName;
    this.patient.middle = patientFormValue.middle;
    this.patient.lastName = patientFormValue.lastName;
    this.patient.dateOfBirth = patientFormValue.dateOfBirth;
    this.patient.socialSecurityNumber = patientFormValue.socialSecurityNumber;
    this.patient.gender = patientFormValue.gender;
    this.patient.phone2 = patientFormValue.phone2;
    this.patient.eMail = patientFormValue.eMail;
    this.patient.patientCode = patientFormValue.patientCode;
    this.patient.user.userName = patientFormValue.user.userName;
    this.patient.user.pasword = patientFormValue.user.pasword;
    this.patient.demographic.adress1 = patientFormValue.demographic.adress1;
    this.patient.demographic.adress2 = patientFormValue.demographic.adress2;
    this.patient.demographic.city = patientFormValue.demographic.city;
    this.patient.demographic.state = patientFormValue.demographic.state;
    this.patient.demographic.zipCode = patientFormValue.demographic.zipCode;
    this.patient.demographic.country = patientFormValue.demographic.country;

    if (event.target.checkValidity()) {
      let apiUrl = `api/patient/${this.patient.idPatient}`;
      this.repository.update(apiUrl, this.patient).subscribe((res) => {
        ($('#successModal') as any).modal({ show: true });
      });
    } else {
      event.target.preventDefault();
      event.target.stopPropagation();
    }
  };
}
