import { Component, OnInit } from '@angular/core';
import { RepositoryService } from './../../shared/services/repository.service';
import {Patient} from './../../interfaces/patient.model';
import { Router } from '@angular/router';


@Component({
  selector: 'app-patient-list',
  templateUrl: './patient-list.component.html',
  styleUrls: ['./patient-list.component.css']
})
export class PatientListComponent implements OnInit {

  public patiens: Patient [] = []; 

  constructor(private repository: RepositoryService, private router: Router) { }

  ngOnInit(): void {
    this.getAllPatients();
  }
  public getAllPatients = () => {
    let apiAddress: string = "api/patient";
    this.repository.getData(apiAddress)
    .subscribe(res => {
      this.patiens = res as Patient[];
    })
  }

  public redirectToUpdatePage = (id:any) => { 
    const updateUrl: string = `/patient/update/${id}`; 
    this.router.navigate([updateUrl]); 
  }

  public redirectToDetailsPage = (id:any) => { 
    const updateUrl: string = `/patient/details/${id}`; 
    this.router.navigate([updateUrl]); 
  }

  public deletePatient = (e:any,id:any) => {
    e.preventDefault(); 
    const updateUrl: string = `api/patient/${id}`; 
    localStorage.setItem('deteleRoute',updateUrl);
    ($('#deleteModal') as any).modal({ show: true });
  }

}
