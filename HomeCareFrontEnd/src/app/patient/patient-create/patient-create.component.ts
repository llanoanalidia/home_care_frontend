import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { RepositoryService } from './../../shared/services/repository.service';
import { patientForCreation } from './../../interfaces/patientForCreation.model';
import { userForCreation } from './../../interfaces/userForCreation.model';
import { Router } from '@angular/router';
import { demographicForCreation } from 'src/app/interfaces/demographicForCreation.model';

@Component({
  selector: 'app-patient-create',
  templateUrl: './patient-create.component.html',
  styleUrls: ['./patient-create.component.css'],
})
export class PatientCreateComponent implements OnInit {
  public patientForm: any;

  constructor(private repository: RepositoryService, private router: Router) {}

  ngOnInit(): void {
    this.patientForm = new FormGroup({
      roleId: new FormControl('', [Validators.required]),
      userName: new FormControl('', [Validators.required]),
      password: new FormControl('', [Validators.required]),
      firstName: new FormControl('', [Validators.required]),
      middle: new FormControl('', [Validators.required]),
      lastName: new FormControl('', [Validators.required]),
      dateOfBirth: new FormControl('', [Validators.required]),
      socialSecurityNumber: new FormControl('', [Validators.required]),
      gender: new FormControl('', [Validators.required]),
      phone1: new FormControl('', [Validators.required]),
      phone2: new FormControl('', [Validators.required]),
      email: new FormControl('', [Validators.required]),
      status: new FormControl('', [Validators.required]),
      patientCode: new FormControl('', [Validators.required]),
      adress1: new FormControl('', [Validators.required]),
      adress2: new FormControl('', [Validators.required]),
      city: new FormControl('', [Validators.required]),
      state: new FormControl('', [Validators.required]),
      zipcode: new FormControl('', [Validators.required]),
      country: new FormControl('', [Validators.required]),
    });
  }
  public createPatient = (event: any, patientFormValue: any) => {
    event.target.classList.add('was-validated');
    if (this.patientForm.valid) {
      this.executePatientCreation(event, patientFormValue);
    }
  };
  public executePatientCreation = (event: any, patientFormValue: any) => {
    switch (patientFormValue.roleId) {
      case 'Admin':
        patientFormValue.roleId = 2;
        break;
      case 'HR':
        patientFormValue.roleId = 3;
        break;
      case 'Nurse':
        patientFormValue.roleId = 4;
        break;
      case 'Coordinator':
        patientFormValue.roleId = 5;
        break;
      case 'HHA':
        patientFormValue.roleId = 6;
        break;
      case 'Patient':
        patientFormValue.roleId = 7;
        break;
      case 'Physician':
        patientFormValue.roleId = 8;
        break;
      case 'Doctor':
        patientFormValue.roleId = 9;
    }

    const user: userForCreation = {
      roleId: 2,
      userName: patientFormValue.userName,
      pasword: patientFormValue.password,
    };

    const demographic: demographicForCreation = {
      adress1: patientFormValue.adress1,
      adress2: patientFormValue.adress2,
      city: patientFormValue.city,
      state: patientFormValue.state,
      zipcode: patientFormValue.zipcode,
      country: patientFormValue.country,
    };

    const patient: patientForCreation = {
      firstName: patientFormValue.firstName,
      middle: patientFormValue.middle,
      lastName: patientFormValue.lastName,
      dateOfBirth: patientFormValue.dateOfBirth,
      socialSecurityNumber: patientFormValue.socialSecurityNumber,
      gender: patientFormValue.gender,
      phone1: patientFormValue.phone1,
      phone2: patientFormValue.phone2,
      eMail: patientFormValue.email,
      status: 'A',
      patientCode: 'P-001',
      user: user,
      demographic: demographic,
    };

    if (event.target.checkValidity()) {
      const apiUrl = 'api/patient';
      this.repository.create(apiUrl, patient).subscribe((res) => {
        ($('#successModal') as any).modal({ show: true });
      });
    } else {
      event.target.preventDefault();
      event.target.stopPropagation();
    }
  };
}
