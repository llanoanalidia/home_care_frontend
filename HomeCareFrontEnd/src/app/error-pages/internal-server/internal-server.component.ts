import { Component, OnInit } from '@angular/core';
@Component({
  selector: 'app-internal-server',
  templateUrl: './internal-server.component.html',
  styleUrls: ['./internal-server.component.css']
})
export class InternalServerComponent implements OnInit {
  public notFoundText: string = `404 SORRY COULDN'T FIND IT!!!`;
  constructor() { }
  ngOnInit() {
  }
}