import { Component, OnInit } from '@angular/core';
import { Employee } from 'src/app/interfaces/employee.model';
import { RepositoryService } from './../../shared/services/repository.service';
import { Router,ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.css']
})
export class EmployeeListComponent implements OnInit {

 public employees: Employee [] = []; 
 
  constructor(private repository: RepositoryService, private router: Router, private activeRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this.getAllEmployees();
  }
  public DeleteEmployee = (e: any, Employee: any) => {
    e.preventDefault();
    localStorage.setItem('deteleRoute',`api/employee/${Employee.idEmployees}`);
    ($('#deleteModal') as any).modal({ show: true });
  };
  public getAllEmployees = () => {
    let apiAddress: string = "api/employee";
    this.repository.getData(apiAddress)
    .subscribe(res => {
      this.employees = res as Employee[];
    })
  }
  public redirectToUpdatePage = (e:any,employee: any) => {
    e.preventDefault();
    let id:any = employee.idEmployees;
    const updateUrl: string = `/employee/update/${id}`;
    this.router.navigate([updateUrl]);
  };

  public redirectToDetailPage = (e:any,employee: any) => {
    e.preventDefault();
    let id:any = employee.idEmployees;
    const detailUrl: string = `/employee/details/${id}`;
    this.router.navigate([detailUrl]);
  };
}
