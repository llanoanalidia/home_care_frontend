import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Employee } from 'src/app/interfaces/employee.model';
import { RepositoryService } from '../../shared/services/repository.service';
import { Router,ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-employee-details',
  templateUrl: './employee-details.component.html',
  styleUrls: ['./employee-details.component.css']
})
export class EmployeeDetailsComponent implements OnInit {
  public employee: any;
  public employeeDetail: any;

  constructor(private repository: RepositoryService,  private router: Router, private activeRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this.employeeDetail = new FormGroup({
      firstName: new FormControl ({value: '', disabled: true}, [Validators.required]),
      middle: new FormControl ({value: '', disabled: true}, [Validators.required]),
      lastName: new FormControl ({value: '', disabled: true}, [Validators.required]),
      dateOfBirth: new FormControl ({value: '', disabled: true}, [Validators.required]),
      npi: new FormControl ({value: '', disabled: true}, [Validators.required]),
      phone1: new FormControl ({value: '', disabled: true}, [Validators.required]),
      phone2: new FormControl ({value: '', disabled: true}, [Validators.required]),
      eMail: new FormControl ({value: '', disabled: true}, [Validators.required]),
      user: new FormGroup ({
          userName: new FormControl ({value: '', disabled: true}, [Validators.required]),
          // pasword: new FormControl ({value: '', disabled: true}, [Validators.required]),
      })
    });
    this.getEmployeeById();
  }
  private getEmployeeById = () => {
    let employeeId: string = this.activeRoute.snapshot.params['id'];
    let employeeByIdUrl: string = `api/employee/${employeeId}`;
    this.repository.getData(employeeByIdUrl)
      .subscribe(res => {
        this.employee = res as Employee;
        console.log("Employee",this.employee)
        this.employeeDetail.patchValue(this.employee);
      },)
  }
}
