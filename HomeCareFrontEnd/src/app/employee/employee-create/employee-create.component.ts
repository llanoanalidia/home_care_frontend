import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { RepositoryService } from './../../shared/services/repository.service';
import { EmployeeForCreation } from './../../interfaces/employeeForCreation.model';
import { userForCreation} from './../../interfaces/userForCreation.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-employee-create',
  templateUrl: './employee-create.component.html',
  styleUrls: ['./employee-create.component.css']
})
export class EmployeeCreateComponent implements OnInit {
  
  public employeeForm: any;
  
  constructor(private repository: RepositoryService,  private router: Router) { }

  ngOnInit(): void {
    this.employeeForm  = new FormGroup ({roleId: new FormControl('', [Validators.required]),
      userName: new FormControl ('', [Validators.required]),
      password: new FormControl ('', [Validators.required]),
      firstName: new FormControl ('', [Validators.required]),
      middle: new FormControl ('', [Validators.required]),
      lastName: new FormControl ('', [Validators.required]),
      dateOfBirth: new FormControl ('', [Validators.required]),
      npi: new FormControl ('', [Validators.required]),
      gender:new FormControl ('', [Validators.required]),
      email: new FormControl ('', [Validators.required]),
      phone1: new FormControl ('', []),
      phone2: new FormControl ('', []),
      address1: new FormControl ('', []),
      address2: new FormControl ('', []),
      city: new FormControl ('', [Validators.required]),
      state: new FormControl ('', []),
      zipcode: new FormControl ('', [Validators.required]),
    });
  }

  public executeEmployeeCreation = (event:any,employeeFormValue : any) => {
    event.target.classList.add('was-validated')
    const user: userForCreation = {
      roleId : 9,
      userName: employeeFormValue.userName,
      pasword: employeeFormValue.password
    }
 
    const employee: EmployeeForCreation = {
      firstName: employeeFormValue.firstName,
      middle: employeeFormValue.middle,
      lastName: employeeFormValue.lastName,
      dateOfBirth: employeeFormValue.dateOfBirth,
      socialSecurityNumber: employeeFormValue.npi,
      gender: employeeFormValue.gender,
      phone1: "3058887271",
      phone2: "zzzzzzz",
      eMail: employeeFormValue.email,
      status:"A",
      Address1: employeeFormValue.address1,
      Address2: employeeFormValue.address2,
      City:employeeFormValue.city,
      State:employeeFormValue.state,
      ZipCode:employeeFormValue.zipcode,
      user:user
    }
    if( event.target.checkValidity() ){
      const apiUrl = 'api/employee';
      this.repository.create(apiUrl, employee)
      .subscribe(res => {
        ($('#successModal') as any ).modal({show:true})
      })
    }else{
      event.target.preventDefault()
      event.target.stopPropagation()
    }
      
  }

}
