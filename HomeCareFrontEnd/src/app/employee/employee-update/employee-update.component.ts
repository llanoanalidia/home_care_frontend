import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { RepositoryService } from './../../shared/services/repository.service';
import { Employee } from 'src/app/interfaces/employee.model';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-employee-update',
  templateUrl: './employee-update.component.html',
  styleUrls: ['./employee-update.component.css'],
})
export class EmployeeUpdateComponent implements OnInit {
  public employeeForm: any;
  public employee: any;

  constructor(
    private repository: RepositoryService,
    private router: Router,
    private activeRoute: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.employeeForm = new FormGroup({
      // roleId: new FormControl('', []),
      userName: new FormControl('', [Validators.required]),
      password: new FormControl('', [Validators.required]),
      firstName: new FormControl('', [Validators.required]),
      middle: new FormControl('', [Validators.required]),
      lastName: new FormControl('', [Validators.required]),
      dateOfBirth: new FormControl('', [Validators.required]),
      npi: new FormControl('', [Validators.required]),
      gender: new FormControl('', [Validators.required]),
      email: new FormControl('', [Validators.required]),
      phone1: new FormControl('', []),
      phone2: new FormControl('', []),
      address1: new FormControl('', []),
      address2: new FormControl('', []),
      city: new FormControl('', [Validators.required]),
      state: new FormControl('', []),
      zipcode: new FormControl('', [Validators.required]),
    });
    this.getEmployeeById();
  }
  private getEmployeeById = () => {
    let employeeId: string = this.activeRoute.snapshot.params['id'];
    let employeeByIdUrl: string = `api/employee/${employeeId}`;
    this.repository.getData(employeeByIdUrl).subscribe((res) => {
      this.employee = res as Employee;
      console.log('Employee', this.employee);
      this.employeeForm.patchValue(this.employee);
    });
  };
  public updateEmployee = (event: any, employeeFormValue: any) => {
    event.target.classList.add('was-validated');
    if (this.employeeForm.valid) {
      this.executeEmployeeUpdate(event, employeeFormValue);
    }
  };

  private executeEmployeeUpdate = (event: any, employeeFormValue: any) => {
    this.employee.firstName = employeeFormValue.firstName;
    this.employee.middle = employeeFormValue.middle;
    this.employee.lastName = employeeFormValue.lastName;
    this.employee.dateOfBirth = employeeFormValue.dateOfBirth;
    this.employee.npi = employeeFormValue.npi;
    this.employee.gender = employeeFormValue.gender;
    this.employee.phone1 = employeeFormValue.phone1;
    this.employee.phone2 = employeeFormValue.phone2;
    this.employee.eMail = employeeFormValue.email;
    this.employee.address1 = employeeFormValue.address1;
    this.employee.address2 = employeeFormValue.address2;
    this.employee.city = employeeFormValue.city;
    this.employee.state = employeeFormValue.state;
    this.employee.zipCode = employeeFormValue.zipcode;
    this.employee.user.userName = employeeFormValue.userName;
    this.employee.user.pasword = employeeFormValue.password;
    if (event.target.checkValidity()) {
      let apiUrl = `api/employee/${this.employee.idEmployees}`;
      this.repository.update(apiUrl, this.employee).subscribe((res) => {
        ($('#successModal') as any).modal({ show: true });
      });
    } else {
      console.log("KLK");
      event.target.preventDefault();
      event.target.stopPropagation();
    }
  };
}
