import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgxMaskModule, IConfig } from 'ngx-mask'
import { EmployeeListComponent } from './employee-list/employee-list.component';
import { EmployeeDetailsComponent } from './employee-details/employee-details.component';
import { RouterModule } from '@angular/router';
import {ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { SharedModule } from './../shared/shared.module';
import { EmployeeCreateComponent } from './employee-create/employee-create.component';
import { EmployeeUpdateComponent } from './employee-update/employee-update.component';

const employeeMaskConfig: Partial<IConfig> = {
  validation: false,
};

@NgModule({
  declarations: [EmployeeListComponent, EmployeeDetailsComponent, EmployeeCreateComponent, EmployeeUpdateComponent],
  imports: [
    CommonModule,
    SharedModule,
    ReactiveFormsModule,
    FormsModule,
    NgxMaskModule.forRoot(employeeMaskConfig),
    RouterModule.forChild([
      { path: 'list', component: EmployeeListComponent },
      { path: 'details/:id', component: EmployeeDetailsComponent },
      { path: 'create', component: EmployeeCreateComponent },
      { path: 'update/:id', component: EmployeeUpdateComponent },
    ])
  ]
})
export class EmployeeModule { }
