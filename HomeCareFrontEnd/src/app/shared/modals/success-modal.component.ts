import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-success-modal',
  templateUrl: './success-modal.component.html',
  styleUrls: ['./success-modal.component.css']
})
export class SuccessModalComponent implements OnInit {
  @Input() public modalHeaderText: string = '';
  @Input() public modalBodyText: string = '';
  @Input() public okButtonText: string = '';
  @Output() public redirectOnOK = new EventEmitter();
  @Input() public routeToRedirect: string = '/home';
  constructor(private router: Router) { }

  ngOnInit() {
  }

  public emitEvent = () => {
    this.router.navigate([this.routeToRedirect]);
    this.redirectOnOK.emit();
  }

}