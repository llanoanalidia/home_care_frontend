import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { RepositoryService } from './../../shared/services/repository.service';

@Component({
  selector: 'app-delete-modal',
  templateUrl: './delete-modal.component.html',
  styleUrls: ['./delete-modal.component.css']
})
export class DeleteModalComponent implements OnInit {
  @Input() public modalHeaderText: string = '';
  @Input() public modalBodyText: string = '';
  @Input() public okButtonText: string = '';
  @Input() public cancelButtonText: string = '';
  @Input() public DeleteButtonText: string = '';
  @Output() public redirectOnOK = new EventEmitter();
  @Output() public redirectOnCancel = new EventEmitter();
  @Output() public redirectOnDelete = new EventEmitter();
  @Input() public routeToRedirect: string = '/home';
  constructor(private router: Router, private repository: RepositoryService) { }

  ngOnInit() {
  }

  public emitOkEvent = () => {
    this.router.navigate([this.routeToRedirect]);
    this.redirectOnOK.emit();
  }

  public emitCancelEvent = () => {
    // this.router.navigate([this.routeToRedirect]);
    this.redirectOnCancel.emit();
  }

  public emitDeleteEvent = () => {
    const deleteEmployee: string = `${localStorage.getItem('deteleRoute')}`;
    this.repository.delete(deleteEmployee).subscribe((res) => {
        // ($('#successModal') as any).modal({ show: true });
      });
    localStorage.removeItem('deteleRoute');
    // this.router.routeReuseStrategy.shouldReuseRoute = () => false;
    // this.router.onSameUrlNavigation = 'reload';
    // this.router.navigate([this.routeToRedirect]);
    window.location.reload();
    this.redirectOnDelete.emit();
  }


}