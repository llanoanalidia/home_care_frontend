import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SuccessModalComponent } from './modals/success-modal.component';
import { DeleteModalComponent } from './modals/delete-modal.component';




@NgModule({
  imports: [
    CommonModule
  ],declarations: [
  SuccessModalComponent,
  DeleteModalComponent
  ],
 // SuccessModalComponent,
  //DatepickerDirective],
  exports: [
    SuccessModalComponent,
    DeleteModalComponent
  ]
})
export class SharedModule { }
