import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { RepositoryService } from './../../shared/services/repository.service';
import { userForCreation } from './../../interfaces/userForCreation.model';
import { Router } from '@angular/router';


@Component({
  selector: 'app-user-create',
  templateUrl: './user-create.component.html',
  styleUrls: ['./user-create.component.css']
})
export class UserCreateComponent implements OnInit {

  public userForm: any;
  
  constructor(private repository: RepositoryService,  private router: Router) {  }

  ngOnInit(): void {
    this.userForm  = new FormGroup ({roleId: new FormControl('', [Validators.required]),
                      userName: new FormControl ('', [Validators.required]),
                      password: new FormControl ('', [Validators.required])
                    }); 


  }
  public createUser = (userFormValue: any) => {
    if (this.userForm.valid) {
      this.executeUserCreation(userFormValue);
    }
  }
  public executeUserCreation = (userFormValue : any) => {
    
    switch (userFormValue.roleId)
    {
      case "Admin": 
        userFormValue.roleId = 2;
        break;
        case "HR":
          userFormValue.roleId = 3;
          break; 
        case "Nurse": 
          userFormValue.roleId = 4;
          break;
        case "Coordinator" :
          userFormValue.roleId = 5;
          break;
        case "HHA": 
          userFormValue.roleId = 6;
          break;
        case "Patient":
          userFormValue.roleId = 7;
          break;
        case "Physician": 
        userFormValue.roleId = 8;
        break;
        case "Doctor":
          userFormValue.roleId = 9;
    } 
    
    const user: userForCreation = {
      userName: userFormValue.userName,
      pasword : userFormValue.password,
      roleId: userFormValue.roleId
    }
    console.log (userFormValue.userName);
    
      const apiUrl = 'api/user';
       this.repository.create(apiUrl, user)
    .subscribe(res => {
      ($('#successModal') as any ).modal({show:true});

    })
    }
}
