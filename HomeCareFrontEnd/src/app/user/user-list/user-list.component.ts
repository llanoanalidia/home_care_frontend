import { Component, OnInit } from '@angular/core';
import { User } from './../../interfaces/user.model';
import { RepositoryService } from './../../shared/services/repository.service';
import { Router } from '@angular/router';
import {ErrorHandlerService } from './../../shared/services/error-handler.service';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {
  
  public users:  User[]  = [];

  constructor(private repository: RepositoryService, private errorHandler : ErrorHandlerService, private router: Router) { 
  
  }

  ngOnInit(): void {
    this.getAllUsers();
  }
  public getAllUsers = () => {
    let apiAddress: string = "api/user";
    this.repository.getData(apiAddress)
    .subscribe(res => {
      this.users = res as User[];
    })
  }

  public getUserDetails = (id = 0) => { 
    const detailsUrl: string = `/user/details/${id}`; 
    this.router.navigate([detailsUrl]); 
  }
}
